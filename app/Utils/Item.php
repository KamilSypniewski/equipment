<?php


namespace App\Utils;


use App\Exceptions\ItemException;

use App\Models\Box;
use App\Models\Reward;
use App\Models\Rune;

use Illuminate\Database\Eloquent\Model;

use Symfony\Component\HttpFoundation\Response;

class Item extends Model
{

    /**
     * @param string $type
     * @param int $id
     * @return Box|Reward|Rune
     * @throws ItemException
     */
    public function getItem(string $type, int $id)
    {
        switch ($type) {
            case Box::NAME:
                $item = new Box();
                break;
            case Reward::NAME:
                $item = new Reward();
                break;
            case Rune::NAME:
                $item = new Rune();
                break;
            default:
                throw new ItemException('Incorrect type of Item.', Response::HTTP_BAD_REQUEST);
        }

        return $item::find($id);
    }
}
