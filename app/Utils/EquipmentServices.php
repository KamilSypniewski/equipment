<?php

namespace App\Utils;

use App\Exceptions\ItemException;
use App\Models\Equipment;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpFoundation\Response;

class EquipmentServices
{
    /**
     * @return array
     * @throws ItemException
     */
    public function list(): array
    {
        $equipments = [];
        foreach (Equipment::all() as $equipment) {
            $item = $equipment->getItem();
            $attribute = $item->getAttributes();
            unset($attribute['created_at'], $attribute['updated_at']);

            $equipments[] = [
                'id' => $equipment->getAttribute('id'),
                'count' => $equipment->getAttribute('count'),
                'item' =>  $attribute
            ];
        }

        return $equipments;
    }

    /**
     * @param string $type
     * @param int $id
     * @return Equipment
     * @throws ItemException
     */
    public function add(string $type, int $id): Equipment
    {
        $equipmentItems = $this->getItem($type, $id);
        if($equipmentItems->isNotEmpty()) {
            $equipment = $this->addCount($equipmentItems);
        } else {
            $equipment = $this->addNew($type, $id);
        }
        $equipment->save();

        return $equipment;
    }

    /**
     * @param $equipmentItems
     * @return mixed
     */
    private function addCount($equipmentItems): Equipment
    {
        $equipment = $equipmentItems->first();
        $equipment->count = (int)$equipment->getAttribute('count') + 1;
        return $equipment;
    }

    /**
     * @param string $type
     * @param int $id
     * @return Equipment
     * @throws ItemException
     */
    private function addNew(string $type, int $id): Equipment
    {
        $equipment = new Equipment();
        $item = (new Item())->getItem($type, $id);

        if (null !== $item) {
            $equipment->count = 1;
            $equipment->item_type = $type;
            $equipment->item_id = $id;
        } else {
            throw new ItemException('Not found Item.', Response::HTTP_NOT_FOUND);
        }

        return $equipment;
    }

    /**
     * @param string $type
     * @param int $id
     * @return Collection
     */
    private function getItem(string $type, int $id): Collection
    {
        return Equipment::where([
            ['item_type', '=', $type],
            ['item_id', '=', $id]
        ])->get();
    }

}
