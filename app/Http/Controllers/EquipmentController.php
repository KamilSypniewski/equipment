<?php

namespace App\Http\Controllers;

use App\Http\Requests\EquipmentCreateRequest;
use App\Utils\EquipmentServices;

use Exception;
use Illuminate\Http\JsonResponse;

class EquipmentController extends Controller
{
    /**
     * @param EquipmentServices $equipmentServices
     * @return JsonResponse
     */
    public function index(EquipmentServices $equipmentServices): JsonResponse
    {
        try {
            return response()->json($equipmentServices->list());
        } catch (Exception $exception) {
            return response()->json(
                ['error'=>$exception->getMessage()],
                $exception->getCode()
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param EquipmentCreateRequest $request
     * @param EquipmentServices $equipmentServices
     * @return JsonResponse
     */
    public function create(EquipmentCreateRequest $request, EquipmentServices $equipmentServices): JsonResponse
    {
        //TODO add validated request
        //$validated = $request->validated();
        $requestData = json_decode($request->getContent());

        try {
            $equipment = $equipmentServices->add($requestData->type, $requestData->id);
            return response()->json([
                'id' => $equipment->getAttribute('id')
            ]);
        } catch (Exception $exception) {
            return response()->json(
                ['error'=>$exception->getMessage()],
                $exception->getCode()
            );
        }
    }
}
