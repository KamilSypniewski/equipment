<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public const  NAME = 'reward';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'price', 'code', 'status_id'
    ];

    public function status()
    {
        return $this->hasMany('App\Models\Status');
    }
}
