<?php

namespace App\Models;

use App\Exceptions\ItemException;
use App\Utils\Item;
use Illuminate\Database\Eloquent\Model;


class Equipment extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'count', 'item_id', 'item_type',
    ];

    /**
     * @return Box|Reward|Rune
     * @throws ItemException
     */
    public function getItem(){
        return (new Item())->getItem(
            $this->item_type, $this->item_id
        );
    }
}
