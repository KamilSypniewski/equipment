<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rune extends Model
{
    public const NAME = 'rune';
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'bonus',
    ];
}
