<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'name'
    ];
}
