<?php

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Status::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Status::create([
            'key'=> 0,
            'name' => 'waiting'
        ]);

        Status::create([
            'key'=> 1,
            'name' => 'sent'
        ]);

        Status::create([
            'key'=> 2,
            'name' => 'rejected'
        ]);

    }
}
