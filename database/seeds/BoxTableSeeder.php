<?php


use App\Models\Box;
use Faker\Factory;
use Illuminate\Database\Seeder;

class BoxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Box::truncate();
        $faker = Factory::create();
        for ($i = 0; $i < 50; $i++) {
            Box::create([
                'image' => $faker->imageUrl(),
                'name' => $faker->name,
                'price' => $faker->numberBetween(5, 200),
            ]);
        }
    }
}
