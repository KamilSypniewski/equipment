<?php

use App\Models\Rune;
use Faker\Factory;
use Illuminate\Database\Seeder;

class RuneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Rune::truncate();

        $faker = Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Rune::create([
                'image' => $faker->imageUrl(),
                'name' => $faker->company,
                'bonus' => $faker->paragraph,
            ]);
        }
    }
}
