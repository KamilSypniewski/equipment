<?php

use App\Models\Reward;
use App\Models\Status;
use Illuminate\Database\Seeder;
use Faker\Factory;

class RewardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reward::truncate();
        $faker = Factory::create();
        for ($i = 0; $i < 50; $i++) {
            Reward::create([
                'image' => $faker->imageUrl(),
                'name' => $faker->companyEmail,
                'code' => $faker->numberBetween(5, 200),
                'price' => $faker->numberBetween(5, 200),
                'status_id' => Status::where('key', '0')->first()->id
            ]);
        }
    }
}
