Przykładowe wywołania api

dodanie przedmiotu do ekwipunku
GET /api/equipment HTTP/1.1
Host: 127.0.0.1:8000
Content-Type: application/json
cache-control: no-cache
Postman-Token: d96f3e70-83a4-4941-941a-4ec1cd96b12b


pobranie listy przedmiotów znajdujących się w ekwipunku
GET /api/equipment HTTP/1.1
Host: 127.0.0.1:8000
Content-Type: application/json
cache-control: no-cache
Postman-Token: bd4b7cf6-cd34-49bc-9a6d-2569fbcd9b63
{
	"type":"rune",
	"id": 1
}
------WebKitFormBoundary7MA4YWxkTrZu0gW--
