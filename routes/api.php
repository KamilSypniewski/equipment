<?php

use App\Models\Reward;
use App\Models\Rune;
Use App\Models\Box;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('equipment', 'EquipmentController@index');
Route::put('equipment', 'EquipmentController@create');

Route::get('box', function() {
    return Box::all();
});

Route::get('rune', function() {
    return Rune::all();
});

Route::get('reward', function() {
    return Reward::all();
});
