Zadanie polega na stworzeniu systemu ekwipunku backend. Ekwipunek składa się z:
a) lista skrzynek
b) lista run 
c) lista nagród 

Wytyczne.
a) Poprawnie zrobione migracje bazodanowe wraz z odpowiednimi relacjami, modele, resource
b) Poprawnie zrobione routes
c) Modele:
Skrzynka posiada atrybuty: nazwa, obrazek, cena
Runa posiada atrybuty: nazwa, obrazek, bonus
Nagroda posiada atrybuty: nazwa, obrazek, kod, cena, status (0-oczekujący, 1-wysłany, 2-odrzucony)

Funkcje
a) Pobranie całego ekwipunku
b) Kupowanie skrzynki, runy, nagrody

Preferowany stack:
- Laravel (latest)
- SQL

Wskazówki:
- Ogranicz ilość API, jednym API można pobrać wszystkie informacje
- Projekt graficzny to pomoc do zobrazowania problemu
- Zadanie to ma na celu zweryfikować Twoje podejście do niektórych problemów, ciekawe rozwiązania, a niekoniecznie przedstawienie "gotowego produktu". Jeśli więc jakiś fragment zajmuje Ci za dużo czasu - pomiń go i przejdź dalej. Chcielibyśmy bowiem, aby zadanie nie zajęło Ci więcej niż 4 godziny.
- Korzystaj z dobrych praktyk. Pamiętaj o rozszerzalności. Mimo, że projekt nie musi być idealnym odwzorowaniem, to jednak potraktuj go tak, jakby miał być użyty i rozwijany w dużym projekcie

